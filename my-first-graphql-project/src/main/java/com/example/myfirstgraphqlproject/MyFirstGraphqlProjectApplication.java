package com.example.myfirstgraphqlproject;

import graphql.Scalars;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.CompletableFuture;

@SpringBootApplication
public class MyFirstGraphqlProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstGraphqlProjectApplication.class, args);
	}

}
