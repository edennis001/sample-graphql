package com.example.myfirstgraphqlproject;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {

    public List<Post> getPosts(String name) {


        Post post = new Post();
        post.name = "mike";
        post.age = 4;

        List<Post> posts = new ArrayList<>();
        posts.add(post);

        return posts;
    }


    class Post {

        public String name;
        public int age;

    }
}